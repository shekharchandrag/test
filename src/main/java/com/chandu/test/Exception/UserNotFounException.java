package com.chandu.test.Exception;

public class UserNotFounException extends RuntimeException {


    public UserNotFounException(String message) {
        super(message);
    }
}
