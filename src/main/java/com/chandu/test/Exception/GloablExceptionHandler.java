package com.chandu.test.Exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GloablExceptionHandler {

    @ExceptionHandler(UserNotFounException.class)
    public ResponseEntity handleUserNotFound(){
        return  new ResponseEntity("User not/ Found", HttpStatus.BAD_REQUEST);
    }
}
