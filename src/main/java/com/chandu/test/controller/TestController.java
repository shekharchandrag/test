package com.chandu.test.controller;

import com.chandu.test.Exception.UserNotFounException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
 @GetMapping("/hello")
    public String sayHello() throws UserNotFounException {

        try {
            return "Hi..Pawan this is demo application for spring boot";
        }
        catch (Exception e){
            throw  new UserNotFounException(e.getMessage());
        }

    }


}
